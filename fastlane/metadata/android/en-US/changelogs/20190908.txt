3.19 - 20190908


== English ==

• User Interface changes
• Preferences: New User Interface for Smartphone, Tablet (fixed menu)
• Preferences: Television Configuration (borders)
• Preferences: Font sizes (5..11)
• Contact menu: Github
• +1 Youtuber (this channel)



== Français ==

• Changements dans l'interface utilisateur
• Préférences: Nouvelle interface utilisateur pour Smartphone, Tablette (menu fixe)
• Préférences: Configuration Télévision (bords)
• Préférences: tailles de police (5..11)
• Menu Contact: Github
• +1 Youtuber (ce channel)



== Italiano ==

• Modifiche nell'interfaccia utente
• Preferenze: nuova interfaccia utente per Smartphone, Tablet (menu fisso)
• Preferenze: Impostazione TV (bordi)
• Preferenze: dimensioni dei caratteri (5..11)
• Contatto: Github
• +1 Youtuber (questo canale)



== Español ==

• Cambios en la interfaz de usuario
• Preferencias: nueva interfaz de usuario para Smartphone, Tableta (menú fijo)
• Preferencias: configuración de TV (bordes)
• Preferencias: tamaños de fuente (5..11)
• Contacto: Github
• +1 Youtuber (este canal)



== Portugués ==

• Alterações na interface do usuário
• Preferências: nova interface de usuário para Smartphone, Tablet (menu fixo)
• Preferências: configurações da TV (bordas)
• Preferências: tamanhos de fonte (5..11)
• Contato: Github
• +1 Youtuber (este canal)
