• No tiene que reiniciar la aplicación cuando cambia algunas preferencias
• El idioma seleccionado en la aplicación puede ser diferente del idioma de su sistema
• Al seleccionar una Biblia, esto definirá el idioma utilizado en la aplicación