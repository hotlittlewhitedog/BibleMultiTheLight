Bíblia em vários idiomas, gratuita, off-line, sem anúncios, totalmente em inglês, francês, italiano, espanhol, português, hindi.

King James Version, Segond, Diodati, Valera, Almeida, Ostervald, Schlachter, Elberfelder, Bíblia romena, Bíblia polaca, Bíblia russa, Bíblia turca, Bíblia suaíli, Bíblia em árabe, Bíblia em hindi, Bíblia bengali, Bíblia chinesa, Bíblia japonesa.

Fácil de usar com funções de pesquisa rápida, compartilhamento, planos de leitura, Bíblia audio, artigos, referências cruzadas, harmonia dos Evangelhos, versos aleatórios, interlinear (Concordância de Strong em inglês).

Também funciona na Android TV, Chromebook.

The Light é uma poderosa ferramenta de estudo para aprender a Palavra de Deus.


