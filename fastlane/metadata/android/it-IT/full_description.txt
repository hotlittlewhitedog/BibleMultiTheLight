Bibbia multi lingue, gratuita, offline, senza pubblicità, interamente in inglese, francese, italiano, spagnolo, portoghese, hindi.

King James Version, Segond, Diodati, Valera, Almeida, Ostervald, Schlachter, Elberfelder, Bibbia rumena, Bibbia polacca, Bibbia russa, Bibbia turca, Bibbia swahili, Bibbia in arabo, Bibbia hindi, Bibbia bengalese, Bibbia cinese, Bibbia japonese.

Facile da usare con funzioni di ricerca veloce e di condivisione, piani de lettura, Bibbia audio, articoli, riferimenti incrociati, armonia dei Vangeli, versi casuali, interlineare (Concordanza di Strong in inglese).

Funziona anche su Android TV, Chromebook.

The Light è un potente strumento di studio per imparare la Parola di Dio.



