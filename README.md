## Web site
https://www.BibleMulti.org

## Description
Bible multi languages, free, offline, no advertising, completely in English, French, Italian, Spanish, Portuguese, Hindi.  

Bibles included: King James Version, Louis Segond, Ostervald, Diodati, Reina Valera, Almeida, Schlachter, Elberfelder, Romanian Bible (Romanian Cornilescu 1928), Polish Bible (Biblia Warszawska 1975), Russian Bible (Russian Synodal Translation 1876), Turkish Bible (New Turkish Bible 2001), Swahili Bible (Swahili Union Version 1997), Arabic Bible (Smith & Van Dyke), Hindi Bible, Bengali Bible (Bengali C.L. Bible 2016), Chinese Bible (Chinese Union Version Simplified), Japanese Bible (New Japanese Bible 1973).   

Easy to use with quick search and share, plans of reading, audio Bible, articles, cross-references, harmony of Gospels, random verses, interlinear (Strongs concordance in English).  
Also works on Android TV, Chromebook.  
The Light is a powerful study tool to learn the Word of God.  

* [To install on Android TV](https://hotlittlewhitedog.gitlab.io/biblemulti/blog/note_android_tv/)


## License
<img src="https://gnu.org/graphics/gplv3-127x51.png" alt="gnuv3" />
[GPLv3](http://www.gnu.org/licenses/gpl-3.0.html) 


## Screenshots
![Screenshot](/fastlane/metadata/android/en-US/images/phoneScreenshots/gaa1.png)
![Screenshot](/fastlane/metadata/android/en-US/images/phoneScreenshots/gaa2.png)
![Screenshot](/fastlane/metadata/android/en-US/images/phoneScreenshots/gaa3.png)
![Screenshot](/fastlane/metadata/android/en-US/images/phoneScreenshots/gaa4.png)
![Screenshot](/fastlane/metadata/android/en-US/images/phoneScreenshots/gaa5.png)
![Screenshot](/fastlane/metadata/android/en-US/images/phoneScreenshots/gaa6.png)
![Screenshot](/fastlane/metadata/android/en-US/images/phoneScreenshots/gaa7.png)
![Screenshot](/fastlane/metadata/android/en-US/images/phoneScreenshots/glb01.png)
![Screenshot](/fastlane/metadata/android/en-US/images/phoneScreenshots/glb02.png)
![Screenshot](/fastlane/metadata/android/en-US/images/phoneScreenshots/glb05.png)
![Screenshot](/fastlane/metadata/android/en-US/images/phoneScreenshots/glb06.png)
![Screenshot](/fastlane/metadata/android/en-US/images/phoneScreenshots/glb07.png)
![Screenshot](/fastlane/metadata/android/en-US/images/phoneScreenshots/glb08.png)
![Screenshot](/fastlane/metadata/android/en-US/images/phoneScreenshots/glb09.png)
![Screenshot](/fastlane/metadata/android/en-US/images/phoneScreenshots/glb10.png)
![Screenshot](/fastlane/metadata/android/en-US/images/phoneScreenshots/glb11.png)
![Screenshot](/fastlane/metadata/android/en-US/images/phoneScreenshots/glb12.png)
![Screenshot](/fastlane/metadata/android/en-US/images/phoneScreenshots/glb13.png)
![Screenshot](/fastlane/metadata/android/en-US/images/phoneScreenshots/glb14.png)
![Screenshot](/fastlane/metadata/android/en-US/images/phoneScreenshots/glb15.png)
![Screenshot](/fastlane/metadata/android/en-US/images/phoneScreenshots/glb16.png)
![Screenshot](/fastlane/metadata/android/en-US/images/phoneScreenshots/glb17.png)
![Screenshot](/fastlane/metadata/android/en-US/images/phoneScreenshots/glb18.png)
